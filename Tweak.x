// 888                                 888               888      d8b
// 888                                 888               888      Y8P
// 888                                 888               888         
// 88888b.   8888b.  88888b.   8888b.  88888b.   8888b.  888  888 888
// 888 "88b     "88b 888 "88b     "88b 888 "88b     "88b 888 .88P 888
// 888  888 .d888888 888  888 .d888888 888  888 .d888888 888888K  888
// 888  888 888  888 888  888 888  888 888  888 888  888 888 "88b 888
// 888  888 "Y888888 888  888 "Y888888 888  888 "Y888888 888  888 888
//
// (c) lillian rose winter 2019-2020   all rights reserved or whateva

#import "Headers.h"

NSString *ctaText = @"hanahaki"; //this is for the custom CTA

%hook NSObject
@interface NSObject (customObject)
- (BOOL)isSettingOn:(NSString *)keyStr;
@end

// grabs preferences

%new
- (BOOL)isSettingOn:(NSString *)keyStr {
	const char *keyStrC = [keyStr cStringUsingEncoding:NSUTF8StringEncoding];
	CFPreferencesAppSynchronize(CFSTR("toys.lily.hanahakiprefs"));
	CFPropertyListRef value = CFPreferencesCopyAppValue(CFStringCreateWithCString(NULL, keyStrC, kCFStringEncodingUTF8), CFSTR("toys.lily.hanahakiprefs"));

	NSString *valueString = [NSString stringWithFormat:@"%@", value];
	NSString *noVal = @"0";

	if ([valueString isEqualToString:noVal]) {
		return NO;
	}
	else {
		return YES;
	}
}
%end

//DOCK BG

%hook SBDockView
- (void)setBackgroundAlpha:(double)arg1 {
	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"dockbg"];

	if (isSettingOn) {
		arg1 = 0;
	}

	else {
		arg1 = 1;
	}

	%orig(arg1);
}
%end

%hook SBFloatingDockPlatterView
- (void)layoutSubviews {
	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"dockbg"];
	
	%orig;

	if(isSettingOn) {
		self.backgroundView = nil;
	}
}
%end

//SCREEN BORDERS

%hook _UIRootWindow
- (void)layoutSubviews {
	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"borders"];
	
	%orig;

	if(isSettingOn) {
		self._continuousCornerRadius = 10;
		self.clipsToBounds = YES;
	}
	return;
}
%end

//ICON LABELS

%hook SBIconView
- (void)_applyIconLabelAlpha:(double)arg1 {
	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"iconlabels"];

	if(isSettingOn) {
		arg1 = 0;
	}

	else {
		arg1 = 1;
	}

	%orig(arg1);
}

- (void)layoutSubviews {
	%orig;

	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"iconlabels"];

	if (isSettingOn) {
		self.iconLabelAlpha = 0;
	}

	else {
		self.iconLabelAlpha = 1;
	}
}
%end

%hook SBIconZoomAnimator
- (void)_applyLabelAlphaFraction:(double)arg1 {
	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"iconlabels"];
	
	if(isSettingOn) {
		arg1 = 0;
	}
	else {
		arg1 = 1;
	}

	%orig(arg1);
}
%end

// CUSTOM CALL TO ACTION (press home to unlock)

%hook SBUICallToActionLabel
- (void)setText:(id)arg1 forLanguage:(id)arg2 animated:(bool)arg3 {
	NSObject *object = [[NSObject alloc] init];
	BOOL isSettingOn = [object isSettingOn:@"customctaction"];

	if (isSettingOn) {
		NSDictionary *pref = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"/var/mobile/Library/Preferences/toys.lily.hanahakiprefs.plist"]];
		ctaText = [[pref objectForKey:@"customctatext"] stringValue];
		return %orig(ctaText, arg2, arg3);
	}	
	else {
		return %orig(arg1, arg2, arg3);
	}
}
%end
