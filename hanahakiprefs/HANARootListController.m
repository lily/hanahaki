#include "HANARootListController.h"

@implementation HANARootListController

- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [self loadSpecifiersFromPlistName:@"Root" target:self];
	}

	return _specifiers;
}

- (void)respring {
	pid_t respringPid;
	char *respringArgv[] = {"/usr/bin/killall", (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) ? "backboardd" : "SpringBoard", NULL};
	posix_spawn(&respringPid, respringArgv[0], NULL, NULL, respringArgv, NULL);
	waitpid(respringPid, NULL, WEXITED);
}

- (void)openTwitter {
	if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=lilyshibe"] options:@{} completionHandler:nil];
	}
	
	else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/lilyshibe"]];
	}
}

- (void)openReddit {
	if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"reddit://"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"reddit:///u/lilyshibe"] options:@{} completionHandler:nil];
	}

	else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://reddit.com/u/lilyshibe"]];
	}
}

@end
