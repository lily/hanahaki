INSTALL_TARGET_PROCESSES = SpringBoard

THEOS_DEVICE_IP = 172.20.10.1

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = hanahaki

hanahaki_FILES = Tweak.x
hanahaki_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk
SUBPROJECTS += hanahakiprefs
include $(THEOS_MAKE_PATH)/aggregate.mk
