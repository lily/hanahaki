#import <UIKit/UIKit.h>
#import <os/log.h>
#import <substrate.h>

// dock bg
@interface _UIBackdropView : UIView {}
@end

@interface SBFloatingDockPlatterView : UIView
	@property (nonatomic, retain) _UIBackdropView * backgroundView;
@end

// corner radius
@interface _UIRootWindow : UIView
	@property (setter=_setContinuousCornerRadius:, nonatomic) double _continuousCornerRadius;
	- (double)_continuousCornerRadius;
	- (void)_setContinuousCornerRadius:(double)arg1;
@end

// icon labels
@interface SBIconView : UIView
	@property (assign,nonatomic) double iconLabelAlpha;
@end
